# News App

News app is built in Angular where you can search news by selected countries and categories.

## NewsAPI

NewsAPI is used to fetch data in this app.

`https://newsapi.org`

## Live Demo

[Check here](https://alpertoy.gitlab.io/news-app/)
