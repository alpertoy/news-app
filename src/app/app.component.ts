import { NewsService } from './services/news.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  newsList: any[] = [];
  loading = false;

  constructor(private newsService: NewsService) {}

  searchNews(parameters: any) {
    this.loading = true;
    this.newsList = [];

    setTimeout(() => {
      this.newsService.getNews(parameters).subscribe(news => {
      this.loading = false;
      this.newsList = news.articles;
      }, err => {
        console.log(err);
        this.loading = false;
      })
    }, 1000);

  }
}
