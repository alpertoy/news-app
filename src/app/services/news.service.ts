import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  getNews(parameters: any): Observable<any> {
    const url = 'https://newsapi.org/v2/top-headlines?country='
    + parameters.country +'&category='+ parameters.category +
    '&apiKey=028d93c532a046e6a540f40e69294381';
    return this.http.get(url);
  }
}
