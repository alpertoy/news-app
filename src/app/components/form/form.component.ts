import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Output() selectedParameters = new EventEmitter<any>();

  selectCountry = 'us';
  selectCategory = 'general';

  categories: any[] = [
    { value: 'general', name: 'General'},
    { value: 'business', name: 'Business'},
    { value: 'entertainment', name: 'Entertainment'},
    { value: 'health', name: 'Health'},
    { value: 'science', name: 'Science'},
    { value: 'sports', name: 'Sports'},
    { value: 'technology', name: 'Technology'}
  ];

  countries: any[] = [
    { value: 'us', name: 'United States'},
    { value: 'gb', name: 'United Kingdom'},
    { value: 'nl', name: 'Netherlands'},
    { value: 'fr', name: 'France'},
    { value: 'ca', name: 'Canada'},
    { value: 'co', name: 'Colombia'},
    { value: 'de', name: 'Germany'}
  ];

  constructor() { }

  ngOnInit(): void {
  }

  searchNews() {
    const parameters = {
      category: this.selectCategory,
      country: this.selectCountry
    }

    this.selectedParameters.emit(parameters);
  }

}
