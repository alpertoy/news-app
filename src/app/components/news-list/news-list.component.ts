import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styles: [
  ]
})
export class NewsListComponent implements OnInit {

  @Input() newsList: any;

  constructor() { }

  ngOnInit(): void {
  }

}
